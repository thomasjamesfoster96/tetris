<?php

require_once("config/db.php");

// class autoloader function, this includes all the classes that are needed by the script
// you can remove this stuff if you want to include your files manually
function autoload($class)
{
    require('classes/' . $class . '.class.php');
}

// automatically loads all needed classes, when they are needed
spl_autoload_register("autoload");


//create a database connection
$db    = new Database();

// start this baby and give it the database connection
$login = new Login($db);
mysql_connect("mysql.1freehosting.com", "u267659503_tetri", "tetris");
mysql_select_db("u267659503_tetri");
$queryStuff = mysql_query("SELECT * FROM  scores LIMIT 100 ORDER BY score DESC");


// base structure
if ($login->displayRegisterPage()) {
        include("views/login/register.php");
} else {
    // are we logged in ?
    if ($login->isUserLoggedIn()) {

  ?>

<html>
	<head>
		<title>Tetris League</title>
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script src="js/tetris/kinetic.js">
    </script>
		<script type="text/javascript" src="js/tetris/tetris.core.js"></script>
		<script type="text/javascript" src="js/tetris/assets.view.js"></script>
		<script type="text/javascript" src="js/tetris/messages.view.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.engine.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.view.js"></script>
		<script type="text/javascript" src="js/Main.js"></script>
    <style>
      #links{
      position: absolute;
top: 50%;
left: 0px;
background-color: #fff;
box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
width: 250px;
height: 50px;
z-index: 900;}
    </style>
    <?php include('views/header/header.php'); ?>
	</head>

  <body style="margin: 0px; background: #fff;" >
    <?php
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
    include("views/login/logged_in.php");
    
    ?>
    <h1>Current Grammar Tetris League Leaderboard</h1>
    <table>
      <thead>
        <tr><th>Name</th><th>Score</th><th>Lines</th><th>Level</th></tr>
      </thead>
      <tbody>
        <?php
        
        while ($row = mysql_fetch_assoc($queryStuff)) {
          echo '<tr>';
          echo '<td>' . $row["username"] . '</td>';
          echo '<td>' . $row["score"] . '</td>';
          echo '<td>' . $row["lines"] . '</td>';
          echo '<td>' . $row["level"] . '</td>';
          echo '</tr>';
        }
        
        
        ?>
      </tbody>
    </table>
    
    <div id="links">
      <a href="/index.php">Play</a>
      <a href="//freetetris.org/play.php">Back to FreeTetris.org</a>
    </div>
	</body>

</html>
<?php
} else {
   ?>

<html>
	<head>
		<title>Tetris League</title>
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script src="js/tetris/kinetic.js">
    </script>
		<script type="text/javascript" src="js/tetris/tetris.core.js"></script>
		<script type="text/javascript" src="js/tetris/assets.view.js"></script>
		<script type="text/javascript" src="js/tetris/messages.view.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.engine.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.view.js"></script>
		<script type="text/javascript" src="js/Main.js"></script>
    <?php include('views/header/header.php'); ?>
	</head>

  <body style="margin: 0px;" >
    <?php
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
    include("views/login/not_logged_in.php");
    ?>
	</body>

</html>
<?php
    }
}
?>
