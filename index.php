<?php

/**
 * Simple Sexy PHP Login Script
 * 
 * A simple PHP Login Script without all the nerd bullshit.
 * Uses PHP SESSIONS, modern SHA256-password-hashing and salting
 * and gives the basic functions a proper login system needs.
 * 
 * @package SimplePHPLogin
 * @author Panique <panique@web.de>
 * @link https://github.com/Panique/PHP-Login/
 * @license GNU General Public License Version 3 
 */

/**
 * Additional notes for experienced PHP guys/girls:
 * This script got a big code makeover in September 2012 to make this script more professional
 * and extendable. The database connection is not created within the login class anymore, now we have own
 * classes for both things: database connection and login process. So you can easily write your
 * own classes while using the main db connection.
 * From now, we create a database connection and pass it to each new object we create
 * (Dependency Injection Pattern, by the way). This might look stupid, but it's really good stuff.
 */

/**
 * include the configs / constants for the database connection
 */
require_once("config/db.php");

// class autoloader function, this includes all the classes that are needed by the script
// you can remove this stuff if you want to include your files manually
function autoload($class)
{
    require('classes/' . $class . '.class.php');
}

// automatically loads all needed classes, when they are needed
spl_autoload_register("autoload");


//create a database connection
$db    = new Database();

// start this baby and give it the database connection
$login = new Login($db);

// base structure
if ($login->displayRegisterPage()) {
        include("views/login/register.php");
} else {
    // are we logged in ?
    if ($login->isUserLoggedIn()) {

  ?>

<html>
	<head>
		<title>Tetris League</title>
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script src="js/tetris/kinetic.js">
    </script>
		<script type="text/javascript" src="js/tetris/tetris.core.js"></script>
		<script type="text/javascript" src="js/tetris/assets.view.js"></script>
		<script type="text/javascript" src="js/tetris/messages.view.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.engine.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.view.js"></script>
		<script type="text/javascript" src="js/Main.js"></script>
    <style>
      #links{
      position: absolute;
top: 50%;
left: 0px;
background-color: #fff;
box-shadow: 0 1px 5px rgba(0, 0, 0, 0.25);
width: 250px;
height: 50px;
z-index: 900;}
    </style>
    <?php include('views/header/header.php'); ?>
	</head>

  <body style="margin: 0px;" >
    <?php
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
    include("views/login/logged_in.php");
    ?>
    
    <div id="tetris" style="height:90%"></div>
    <div id="links">
      <a href="/leaderboard.php">Leader Board</a>
      <a href="//freetetris.org/play.php">Back to FreeTetris.org</a>
    </div>
	</body>

</html>
<?php
} else {
   ?>

<html>
	<head>
		<title>Tetris League</title>
		<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
		<script src="js/tetris/kinetic.js">
    </script>
		<script type="text/javascript" src="js/tetris/tetris.core.js"></script>
		<script type="text/javascript" src="js/tetris/assets.view.js"></script>
		<script type="text/javascript" src="js/tetris/messages.view.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.engine.js"></script>
		<script type="text/javascript" src="js/tetris/tetris.view.js"></script>
		<script type="text/javascript" src="js/Main.js"></script>
    <?php include('views/header/header.php'); ?>
	</head>

  <body style="margin: 0px;" >
    <?php
    // the user is logged in. you can do whatever you want here.
    // for demonstration purposes, we simply show the "you are logged in" view.
    include("views/login/not_logged_in.php");
    ?>
	</body>

</html>
<?php
    }
}
